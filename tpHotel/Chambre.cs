﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Chambre
    {
        int id;
        int etage;
        string description;
        int hotel;


        //constructeur
        public Chambre(int unid, int unetage, string unedescription, int unhotel)
        {
            Id = unid;
            etage = unetage;
            description = unedescription;
            hotel = unhotel;


        }

     
        public int Id { get => id; set => id = value; }
        public int Etage { get => etage; set => etage = value; }
        public string Description { get => description; set => description = value; }
        public int Hotel { get => hotel; set => hotel = value; }
    }
}
