﻿namespace tpHotel
{
    partial class frmAccueil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnChambre = new System.Windows.Forms.Button();
            this.btnVoir = new System.Windows.Forms.Button();
            this.btnAjoutChambre = new System.Windows.Forms.Button();
            this.btnQuit = new System.Windows.Forms.Button();
            this.bntChmb = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnChambre
            // 
            this.btnChambre.Location = new System.Drawing.Point(79, 33);
            this.btnChambre.Name = "btnChambre";
            this.btnChambre.Size = new System.Drawing.Size(124, 34);
            this.btnChambre.TabIndex = 0;
            this.btnChambre.Text = "Ajout hotels";
            this.btnChambre.UseVisualStyleBackColor = true;
            this.btnChambre.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnVoir
            // 
            this.btnVoir.Location = new System.Drawing.Point(79, 90);
            this.btnVoir.Name = "btnVoir";
            this.btnVoir.Size = new System.Drawing.Size(124, 36);
            this.btnVoir.TabIndex = 1;
            this.btnVoir.Text = "Visualiser hotels";
            this.btnVoir.UseVisualStyleBackColor = true;
            this.btnVoir.Click += new System.EventHandler(this.btnVisualise_Click);
            // 
            // btnAjoutChambre
            // 
            this.btnAjoutChambre.Location = new System.Drawing.Point(79, 150);
            this.btnAjoutChambre.Name = "btnAjoutChambre";
            this.btnAjoutChambre.Size = new System.Drawing.Size(124, 35);
            this.btnAjoutChambre.TabIndex = 2;
            this.btnAjoutChambre.Text = "Ajout chambre";
            this.btnAjoutChambre.UseVisualStyleBackColor = true;
            this.btnAjoutChambre.Click += new System.EventHandler(this.btnAjoutChambre_Click);
            // 
            // btnQuit
            // 
            this.btnQuit.Location = new System.Drawing.Point(189, 333);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(97, 35);
            this.btnQuit.TabIndex = 3;
            this.btnQuit.Text = "Quitter";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // bntChmb
            // 
            this.bntChmb.Location = new System.Drawing.Point(79, 209);
            this.bntChmb.Name = "bntChmb";
            this.bntChmb.Size = new System.Drawing.Size(124, 39);
            this.bntChmb.TabIndex = 4;
            this.bntChmb.Text = "Visualiser Chambre";
            this.bntChmb.UseVisualStyleBackColor = true;
            this.bntChmb.Click += new System.EventHandler(this.bntChmb_Click);
            // 
            // frmAccueil
            // 
            this.ClientSize = new System.Drawing.Size(298, 380);
            this.Controls.Add(this.bntChmb);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.btnAjoutChambre);
            this.Controls.Add(this.btnVoir);
            this.Controls.Add(this.btnChambre);
            this.Name = "frmAccueil";
            this.Text = "Accueil";
            this.Load += new System.EventHandler(this.frmAccueil_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAjoutHotel;
        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.Button btnVoirHotel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnChambre;
        private System.Windows.Forms.Button btnVoir;
        private System.Windows.Forms.Button btnAjoutChambre;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.Button bntChmb;
    }
}